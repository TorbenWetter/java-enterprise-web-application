<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html>
<html>
<head>
    <title>Systemzeit mit JSP</title>
</head>
<body>
<%
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    String dateString = sdf.format(new Date());
    out.println(dateString);
%>
</body>
</html>